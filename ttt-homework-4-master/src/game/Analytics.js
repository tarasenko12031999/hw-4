const path = require('path')
const fs = require('fs')
const { SCOREFILE } = require('../constants/index')

class Analytics {
  constructor() {
    this.filePath = path.join(process.cwd(), './audit', SCOREFILE)
  }

  addScore(userName) {
    let today = new Date()
    let dd = today.getDate()
    let mm = today.getMonth() + 1
    let yyyy = today.getFullYear()
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    today = dd + '-' + mm + '-' + yyyy
    try {
      fs.appendFileSync(this.filePath, `\n${userName.toString()},${today}`)
    } catch (err) {
      throw err
    }
  }

  printScores() {
    let users = []
    const fileContent = fs.readFileSync(this.filePath).toString().split('\n')
    let data = []
    for (let i = 0; i < fileContent.length; i++) {
      data.push(fileContent[i])
    }
    data.forEach((el) => {
      let user = {}
      el = el.split(',')

      Object.assign(user, { username: el[0], date: el[1] })
      users.push(user)
    })
    console.log(users)
  }
}

module.exports = Analytics
