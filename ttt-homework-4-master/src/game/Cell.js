class Cell {
  constructor(num) {
    this.cellNum = num
    this.isFree = true
  }
}

module.exports = Cell
